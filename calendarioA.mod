
### ---------------- INSIEMI ---------------- ###

set Squadre;
set Stadio;
set Incontro within {Squadre, Squadre, Stadio};
set Giornate;
set Eventi within {Giornate, Stadio};
set Campo within {Squadre, Stadio};


### ---------------- PARAMETRI ---------------- ###
param num_squadre > 0;
param num_giornate >0;
param peso {Incontro, Giornate} > 0;

### ---------------- VARIABILI ---------------- ###

var X {Incontro,Giornate} binary;

### ---------------- VINCOLI ---------------- ###


### Max 2 incontri per giornata
subject to S1 {z in Giornate}: sum {(i,j,s) in Incontro} X[i,j,s,z] = (num_squadre/2);

### Una squadra può giocare una sola volta a giornata
subject to S2 {z in Giornate, i in Squadre} : sum{(i,j,s) in Incontro} X[i,j,s,z] +sum{(j,i,u) in Incontro} X[j,i,u,z]=1;

### Ogni incontro è unico e deve svolgersi in una sola giornata
subject to S3 {(i,j,s) in Incontro} : sum{z in Giornate} X[i,j,s,z]=1;	

### Un solo incontro per stadio a giornata
subject to S4 {z in Giornate, s in Stadio} : sum{(i,j,s) in Incontro} X[i,j,s,z]<=1;

### Per ogni evento non ci deve essere un incontro
subject to S5 {(z,s) in Eventi}: sum{(i,j,s) in Incontro} X[i,j,s,z] = 0;

### Una squadra può giocare in casa consecutivamente al massimo 2 volte di fila
subject to S6 {z in {1..(num_giornate-2)}, i in Squadre}: sum{(i,j,s) in Incontro} ( X[i,j,s,z] + X[i,j,s,z+1] + X[i,j,s,z+2])<=2;

### Una squadra può giocare fuori casa consecutivamente al massimo 2 volte di fila
subject to S7 {z in {1..(num_giornate-2)}, j in Squadre}: sum{(i,j,s) in Incontro} ( X[i,j,s,z] + X[i,j,s,z+1] + X[i,j,s,z+2])<=2;

### La partita giocata all'andata deve ripetersi a campo invertito nel girone di ritorno
subject to S8 {z in {1..(num_giornate/2)},(i,j,u) in Incontro, (j,i,s) in Incontro : i<>j} : X[i,j,u,z] = X[j,i,s,z+(num_giornate/2)];
subject to S9 {z in {1..(num_giornate/2)},(i,j,u) in Incontro, (j,i,s) in Incontro : i<>j} : X[i,j,u,z+(num_giornate/2)] = X[j,i,s,z]; 

### ---------------- OBIETTIVO ---------------- ###

minimize partite_consecutive :  sum {(i,j,s) in Incontro} sum {r in Squadre: r<>i and r<>j} sum {z in {1..(num_giornate/2)}} peso[i,j,s,z] * (X[i,j,s,z] + X[i,r,s,z+1]);
