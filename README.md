## Il progetto

Realizzare un modello matematico per definire le partite di andata e ritorno di un campionato di calcio tenendo in considerazione i seguenti vincoli:
* Limitare al massimo a 2 partite consecutive in casa o in trasferta per ogni singola squadra
* Coppie di squadre che condividono lo stesso impianto sportivo non potranno giocare in casa contemporaneamente
* A causa di eventi concomitanti, certe squadre non potranno giocare in casa in certe giornate prefissate

L'obiettivo è quello di minimizzare il numero di partite consecutive giocate in casa o in trasferta di ogni squadra.
Si tratta di un problema di programmazione lineare.

Per tutti i dettagli implementativi e maggiori dettagli potete leggere il file in formato .pdf __"Relazione AMPL"__